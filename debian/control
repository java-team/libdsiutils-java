Source: libdsiutils-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>
Build-Depends: debhelper-compat (= 13),
               ant,
               javahelper,
               maven-repo-helper,
               ivy,
               default-jdk-headless,
               liblogback-java,
               libfastutil-java,
               libcommons-math3-java,
               libcommons-io-java,
               libcommons-configuration2-java,
               libcommons-collections4-java,
               libjsap-java,
               libslf4j-java,
               libguava-java,
               junit4 <!nocheck>,
               ant-optional <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/java-team/libdsiutils-java
Vcs-Git: https://salsa.debian.org/java-team/libdsiutils-java.git
Homepage: https://dsiutils.di.unimi.it
Rules-Requires-Root: no

Package: libdsiutils-java
Architecture: all
Depends: ${misc:Depends},
         ${java:Depends},
         liblogback-java,
         libfastutil-java,
         libcommons-math3-java,
         libcommons-io-java,
         libcommons-configuration-java,
         libcommons-collections3-java,
         libjsap-java,
         libslf4j-java,
         libguava-java
Description: Java library of statistical and text data handling tools
 The library contains:
  - Implementations of pseudorandom number generators;
  - BitVector and its implementations -- a high-performance but flexible set of
  bit vector classes;
  - A it.unimi.dsi.compression package containing codecs for several types of
  encodings;
  - ProgressLogger, a flexible logger with statistics marking the progress of
  the classes that are used and that require hours of computation;
  - ObjectParser, a class making it easy to specify complex objects on the
  command line;
  - MutableString, an alternative to the immutable Java String class;
  - The I/O package, containing fast version of several classes existing in
  java.io, many useful classes to read easily text data
  (e.g., FileLinesCollection), bit streams, classes providing large-size memory
  mapping such as ByteBufferInputStream, and OfflineIterable -- the easy & fast
  way to store large sequences of objects on disk and iterate on them;
  - The it.unimi.dsi.util package, containing pseudorandom number generators,
  tries, immutable prefix maps, Bloom filters, a very comfortable Properties
  class and more;
  - The it.unimi.dsi.stat package, containing a lightweight class for computing
  basic statistics and an arbitrary-precision implementation of the Jackknife
  method;
  - Lots of utility methods in Util;
  - Big versions of I/O and utility classes in it.unimi.dsi.big.io and
  it.unimi.dsi.big.util;
  - The BulletParser, used to parse HTML and XML.
