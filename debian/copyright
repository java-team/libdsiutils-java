Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DSI Utilities
Source: http://dsiutils.di.unimi.it/
Files-Excluded: bnd
Comment: jars are embedded into the upstream archive.

Files: *
Copyright: 2002-2023 Sebastiano Vigna
License: LGPL-2.1+ or Apache-2.0

Files: src/it/unimi/dsi/io/DebugOutputBitStream.java
       src/it/unimi/dsi/io/WordReader.java
       src/it/unimi/dsi/lang/*
       src/it/unimi/dsi/util/HyperLogLogCounterArray.java
       src/it/unimi/dsi/util/Interval.java
       src/it/unimi/dsi/util/Intervals.java
       src/it/unimi/dsi/util/LongInterval.java
       src/it/unimi/dsi/util/LongIntervals.java
       src/it/unimi/dsi/util/TextPattern.java
       test/it/unimi/dsi/util/HyperLogLogCounterArrayTest.java
Copyright: 2002-2023 Paolo Boldi and Sebastiano Vigna
License: LGPL-2.1+ or Apache-2.0

Files: src/it/unimi/dsi/util/CircularCharArrayBuffer.java
Copyright: 2006-2023 Paolo Boldi
License: LGPL-2.1+ or Apache-2.0

Files: debian/*
Copyright: 2020-2023 Pierre Gruet <pgt@debian.org>
License: LGPL-2.1+ or Apache-2.0

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in ‘/usr/share/common-licenses/LGPL-2.1’.

License: Apache-2.0
 On Debian systems the full text of the Apache-2.0 license can be found in
 /usr/share/common-licenses/Apache-2.0.
